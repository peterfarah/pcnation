﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class member : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Panel1.Visible = false;
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (Request.QueryString["user"] != null)
        {
            if (Session["username"] != null)
            {
                Panel1.Visible = true;
            }
            else
            {
                Panel1.Visible = false;
            }
            string statement = "SELECT * FROM users WHERE username='" + Request.QueryString["user"].ToString() + "'";
            DataSet set = SQL.SelectRecords(statement);
            if (set.Tables[0].Rows.Count > 0)
            {
                name.Text = set.Tables[0].Rows[0]["firstname"].ToString() + " " + set.Tables[0].Rows[0]["lastname"].ToString();
                gender.Text = set.Tables[0].Rows[0]["gender"].ToString();
                email.Text = set.Tables[0].Rows[0]["email"].ToString();
                country.Text = set.Tables[0].Rows[0]["country"].ToString();
                age.Text = set.Tables[0].Rows[0]["age"].ToString();
                Image1.ImageUrl = Constants.AVATARS_IMAGE_PATH + set.Tables[0].Rows[0]["avatar"].ToString();
            }
            bindVisitorsList();
        }
        else
        {
            Session["error"] = "The member you are looking for doesn't exist.";
            Response.Redirect("error.aspx");
        }
    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        if (this.TextBox3.Text.Length > 0)
        {
            string statement = "INSERT INTO visitors (username,message,sender,msgdate) VALUES('" + Request.QueryString["user"].ToString() + "','" + TextBox3.Text + "','" + Session["username"].ToString() + "','" + (DateTime.Today.ToString()) + "')";
            SQL.DoCommand(statement);
            bindVisitorsList();
            TextBox3.Text = "";
        }
    }

    private void bindVisitorsList()
    {
        string statement = "SELECT * FROM visitors WHERE username='" + Request.QueryString["user"].ToString() + "' ORDER BY ID DESC";
        DataList1.DataSource = SQL.SelectRecords(statement).Tables[0];
        DataList1.DataBind();
    }

    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        Response.Redirect("member.aspx?user=" + this.DataList1.DataKeys[e.Item.ItemIndex]);
    }
}