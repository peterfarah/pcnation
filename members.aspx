﻿<%@ Page Title="" Language="C#" MasterPageFile="admin.master" AutoEventWireup="true" CodeFile="members.aspx.cs" Inherits="admin_members" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table align="center" style="background: url('imgs\/style\/adminbody.png');" width="788" align="center">
        <tr>
            <td colspan="3" align="center">MEMBERS CONTROL PANEL
            </td>
        </tr>
        <tr>
            <td align="right">Usernmae:</td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="Button1" runat="server" Text="Search" OnClick="Button1_Click" />
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3" style="padding:10px; border:solid 1px gray;">
                <asp:DataList ID="DataList1" runat="server" Width="75%" DataKeyField="username" OnDeleteCommand="DataList1_DeleteCommand" OnEditCommand="DataList1_EditCommand">
                    <ItemTemplate>
                        <table align="left" width="100%">
                            <tr>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "ID") %>
                                </td>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "username") %>
                                </td>
                                <td align="right">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="edit">Edit</asp:LinkButton>
                                    |
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="delete">Delete</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>

