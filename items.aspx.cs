﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class items : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (Session["admincp"] == null)
            Response.Redirect("alogin.aspx");
        bindMemberList("");
    }

    private void bindMemberList(string u)
    {
        string s = "";
        if (u.Length > 0)
        {
            s = "SELECT * FROM items WHERE itemname='" + u + "'";
        }
        else
        {
            s = "SELECT * FROM items";
        }
        DataList1.DataSource = SQL.SelectRecords(s).Tables[0];
        DataList1.DataBind();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (TextBox1.Text.Length > 0)
            bindMemberList(TextBox1.Text);
    }
    protected void DataList1_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        string s = "DELETE FROM items WHERE ID=" + DataList1.DataKeys[e.Item.ItemIndex];
        SQL.DoCommand(s);
        bindMemberList("");
    }
    protected void DataList1_EditCommand(object source, DataListCommandEventArgs e)
    {
        Response.Redirect("itemedit.aspx?id=" + DataList1.DataKeys[e.Item.ItemIndex]);
    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        Response.Redirect("itemedit.aspx");    
    }
}