﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Timers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Default2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        SortedDictionary<string, string> objDic = new SortedDictionary<string, string>();

        foreach (CultureInfo ObjectCultureInfo in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
        {
            RegionInfo objRegionInfo = new RegionInfo(ObjectCultureInfo.Name);
            if (!objDic.ContainsKey(objRegionInfo.EnglishName))
            {
                objDic.Add(objRegionInfo.EnglishName, ObjectCultureInfo.Name);
            }
        }

        foreach (KeyValuePair<string, string> val in objDic)
        {
            this.DropDownList2.Items.Add(new ListItem(val.Key, val.Value));
        }

        this.errors.Text = "";
        this.Label1.Text = "";
        Panel1.Visible = false;
        if (!this.IsPostBack)
            this.Label4.Text = Util.RandomString(6);
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (this.IsValid)
        {
            if (SQL.SelectRecords("SELECT * FROM users WHERE username='" + TextBox3.Text + "'").Tables[0].Rows.Count > 0)
            {
                errors.Text += "This username already exists. <br>";
            }
            if (SQL.SelectRecords("SELECT * FROM users WHERE email='" + TextBox6.Text + "'").Tables[0].Rows.Count > 0)
            {
                errors.Text += "This email already exists. <br>";
            }
            if (this.TextBox4.Text != this.TextBox5.Text)
            {
                errors.Text += "Passwords didn't match! <br>";

            }
            if (this.TextBox1.Text != this.Label4.Text)
            {
                this.Label1.Text += "Validation text didn't match the image. <br>";

            }
            if (!this.CheckBox1.Checked)
            {
                this.Label1.Text += "You must agree on terms of service. <br>";

            }
            if (errors.Text.Length <= 0)
            {
                string gender = (RadioButton1.Checked) ? "Male" : "Female";
                int age = int.Parse(this.TextBox9.Text);
                string statement = "INSERT INTO users(username,[password],[email],firstname,lastname,country,gender,age,avatar,admin) VALUES('" + TextBox3.Text + "','" + this.TextBox4.Text + "','" + this.TextBox6.Text + "','" + this.TextBox7.Text + "','" + this.TextBox8.Text + "','" + this.DropDownList2.SelectedItem + "','" + gender + "'," + age + ",'avatar.gif',0)";
                SQL.DoCommand(statement);
                return;
            }
            else
            {
                this.Label4.Text = Util.RandomString(6);
                Panel1.Visible = true;
                return;

            }
        }
        else
        {
            this.Label4.Text = Util.RandomString(6);
            Panel1.Visible = true;
            return;
        }

    }
}