﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="message.aspx.cs" Inherits="message" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="background-color:RGB(238,238,238); padding-bottom:4px; padding-top:15px;" border-right:1px solid gray; border-left:1px solid gray;" cellpadding="0" cellspacing="0" width="766">
        <tr>
            <td align="center">
                <table width="500" style="border: solid 1px gray;">
                    <tr>
                        <td>
                            <asp:LinkButton ID="LinkButton7" runat="server" OnClick="LinkButton7_Click"><- Go back to mail
                            </asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td height="38" style="background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">
                            Send message
                            </td>
                    </tr>
                    <tr>
                        <td >
                            To : <asp:TextBox ID="TextBox3" runat="server" Width="300px"></asp:TextBox>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            Seperate the usernames by ',' to send to more than 1 user
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Subject : 
                            <asp:TextBox ID="TextBox4" runat="server" Width="300px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>

                            <asp:TextBox ID="TextBox5" runat="server" Height="150px" TextMode="MultiLine" Width="376px"></asp:TextBox>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button1" runat="server" Text="Send message" OnClick="Button1_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

