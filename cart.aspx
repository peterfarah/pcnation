﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="cart.aspx.cs" Inherits="cart" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="background-color:RGB(238,238,238); padding-bottom:4px; padding-top:15px;" border-right:1px solid gray; border-left:1px solid gray;" cellpadding="0" cellspacing="0" width="766">
        <tr>
            <td align="center">
               <table width="550" style="border:dashed 1px #2c6aff;">
                   <tr>
                       <td height="38" style="background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">
                           Your cart
                       </td>
                   </tr>
                   <tr> 
                       <td>
                           <asp:DataList ID="DataList1" runat="server" Width="100%" DataKeyField="ID" OnDeleteCommand="DataList1_DeleteCommand" OnEditCommand="DataList1_EditCommand" OnItemCommand="DataList1_ItemCommand">
                               <ItemTemplate>
                                    <table align="left" width="100%">
                                        <tr bgcolor="darkgray">
                                            <td align="left"><%#DataBinder.Eval(Container.DataItem, "itemname") %></td>
                                            <td align="left"><%#DataBinder.Eval(Container.DataItem, "qty") %></td>
                                            <td align="right"><%#DataBinder.Eval(Container.DataItem, "price") %></td>
                                            <td>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CommandName="select">+</asp:LinkButton> |
                                                <asp:LinkButton ID="LinkButton2" runat="server" CommandName="edit">-</asp:LinkButton> |
                                                <asp:LinkButton ID="LinkButton3" runat="server" CommandName="delete">Delete</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </table>
                               </ItemTemplate>
                           </asp:DataList>
                       </td>
                   </tr>
                   <tr bgcolor="gray">
                       <td align="right"> Total price : 
                           <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                           $</td>
                   </tr>
                   <tr bgcolor="gray">
                       <td align="right"> 
                           <asp:Button ID="Button2" runat="server" Text="Checkout" />
                       </td>
                   </tr>
                   <tr>
                       <td></td>
                   </tr>
               </table>
            </td>
        </tr>
    </table>
</asp:Content>

