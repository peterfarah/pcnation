﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class mail : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (Session["username"] == null)
        {
            Response.Redirect("login.aspx");
        } if (!IsPostBack)
            Session["mail"] = "inbox";
        bindMessagesList();
    }

    private void bindMessagesList()
    {
        string s;
        if (Session["mail"].ToString() == "inbox")
        {
            s = "SELECT * FROM [messages] WHERE receiver='" + Session["username"].ToString() + "' ORDER BY ID DESC";
        }
        else
        {
            s = "SELECT * FROM [messages] WHERE username='" + Session["username"].ToString() + "' ORDER BY ID DESC";
        }
        DataList1.DataSource = SQL.SelectRecords(s).Tables[0];
        DataList1.DataBind();
    }

    protected void LinkButton7_Click(object sender, EventArgs e)
    {
        Session["mail"] = "inbox";
        bindMessagesList();
    }

    protected void LinkButton8_Click(object sender, EventArgs e)
    {
        Session["mail"] = "outbox";
        bindMessagesList();
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        Response.Redirect("read.aspx?id=" + this.DataList1.DataKeys[e.Item.ItemIndex].ToString());
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("message.aspx");
    }
}