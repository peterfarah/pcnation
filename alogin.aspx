﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="alogin.aspx.cs" Inherits="alogin" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 30px;
        }
        .auto-style2 {
            height: 26px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <table cellspacing="0" cellpadding="0" width="500" style="margin: 15px; background-color: RGB(238,238,238); padding-bottom: 4px; border-right: 1px solid gray; border-left: 1px solid gray; border: solid 1px gray;">
        <tr>
            <td align="center" height="38" style="background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">Administrator login
            </td>
        </tr>
        <tr>
            <td align="center" valign="top">
                <table width="100%">
                    <tr>
                        <td colspan="3">Please enter your admin username and password
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style2">Username: </td>
                        <td class="auto-style2">
                            <asp:TextBox ID="usernameTxt1" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Password: </td>
                        <td>
                            <asp:TextBox ID="passTxt1" runat="server" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" class="auto-style1">
                            <asp:Button ID="Button2" runat="server" Text="Login" Width="150" OnClick="Button2_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        <tr>
            <td>
                <asp:Label ID="errorLabels" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

