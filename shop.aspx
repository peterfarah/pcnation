﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="shop.aspx.cs" Inherits="shop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="background-color:RGB(238,238,238); padding-bottom:4px; border-right:1px solid gray; border-left:1px solid gray;" cellpadding="0" cellspacing="0" height="380" width="766">
        <tr>
            <td colspan="2" height="10"></td>
        </tr>

        <tr>
            <td width="205" valign="top">
                <table style="margin-left:3px; border: 1px solid gray;" cellpadding="0" cellspacing="0" align="left">

                    <tr>
                        <td colspan="" height="30" style="border: 1px solid gray; background: url('imgs\/style\/cattopbg.png') repeat-x;">Categories</td>
                    </tr>
                    <tr>
                        <td bgcolor="RGB(246,246,248)" align="left" width="205">
                            <asp:DataList ID="DataList1" runat="server" DataKeyField="ID" OnItemCommand="DataList1_ItemCommand">
                                <ItemTemplate>
                                    <table width="100%">
                                        <tr>
                                            <td>
                                                <img src="imgs/style/catarrow.png" border="0" width="11" height="11" /><asp:LinkButton ID="LinkButton1" runat="server" Font-Names="Khmer UI" Text='<%#DataBinder.Eval(Container.DataItem, "catname") %>' Font-Underline="False">LinkButton</asp:LinkButton></td>
                                        </tr>
                                        <tr>
                                            <td height="1">
                                                <hr style="border-bottom: 1px dotted black;"></hr>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </td>
            <td  rowspan="2" valign="top">
                <table align="left">
                    <tr>
                        <td width="2"></td>
                        <td>
                            <asp:DataList ID="DataList2" runat="server" RepeatColumns="2" DataKeyField="ID" OnItemCommand="DataList2_ItemCommand">
                                <ItemTemplate>
                                    <table height="128" width="272" align="left" style="border: 1px solid gray;" bgcolor="RGB(246,246,248)">
                                        <tr >
                                            <td style=" padding-left:7px; width:100px; height:86px;" rowspan="3" >
                                                <asp:Image ID="Image1" Width="100" Height="86" runat="server" ImageUrl='<%#DataBinder.Eval(Container.DataItem, "itempic", "imgs/items/{0}") %>' BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px"/></td>
                                            <td width="5" rowspan="3"></td>
                                            <td><asp:Label ID="Label4" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ID") %>' Visible="false"></asp:Label><asp:Label ID="Label1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "itemname") %>'></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><asp:Label ID="Label2" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "itemdesc") %>'></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td><asp:Label ID="Label3" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "price") %>'></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td height="20" colspan="3">
                                                <asp:ImageButton ID="ImageButton1" CommandName="select" ImageUrl="~/imgs/style/details.png" runat="server" /></td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

