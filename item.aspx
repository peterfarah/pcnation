﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="item.aspx.cs" Inherits="item" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table width="765" style="padding: 15px; border: solid 1px darkgray;" cellspacing="0" cellpadding="0">
        <tr>
            <td width="150" height="150" style="border: solid 1px darkgray;">
                <asp:Image ID="Image1" Width="150" Height="150" runat="server"/>
            </td>
            <td width="15"></td>
            <td valign="top" width="615" align="left">
                <table width="100%">
                    <tr>
                        <td>Item name: <asp:Label ID="nameLabel" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td height="15"></td>
                    </tr>
                    <tr>
                        <td>Description:
                            <asp:Label ID="descLabel" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <td align="left">
                <table width="100%">
                    <tr>
                        <td>PRICE: <asp:Label ID="priceLabel" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td align="right">
                            <asp:Button ID="Button2" runat="server" Text="Add to cart!" OnClick="Button2_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr><td colspan="3"><hr /></td></tr>
        <tr>
            <td colspan="3" align="center">
                <asp:Panel ID="Panel1" runat="server">
                    <table width="400">
                        <tr>
                            <td colspan="2">Add a user review about this product:</td>
                        </tr>
                        <tr>
                            <td>your review<br />
                            </td><td>
                            <asp:TextBox ID="TextBox3" runat="server" TextMode="MultiLine" Height="41px" Width="300px" MaxLength="250"></asp:TextBox>
                            </td>
                        </tr>
                        <tr><td colspan="2">
                            <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" /></td></tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr><td colspan="3"><hr /></td></tr>
        <tr>
            <td colspan="3">
                <asp:DataList ID="DataList1" runat="server" Width="735" DataKeyField="username" OnItemCommand="DataList1_ItemCommand">
                    <ItemTemplate>
                        <table align="left" bgcolor="#dcdcdc" style="border:dashed darkgray 1px; table-layout:fixed;">
                            <tr>
                                <td align="left"><asp:LinkButton runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "username") %>' CommandName="select"></asp:LinkButton></td>
                                <td align="right">Date: <%#DataBinder.Eval(Container.DataItem, "commentdate") %></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="width:735px;" colspan="2" align="left">
                                   <%#DataBinder.Eval(Container.DataItem, "comment") %>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>

