﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table cellspacing="0" cellpadding="0" width="500" style="margin: 15px; background-color: RGB(238,238,238); padding-bottom: 4px; border-right: 1px solid gray; border-left: 1px solid gray; border: solid 1px gray;">
        <tr>
            <td align="center" colspan="3" height="38" style="background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">Please choose one of the given options
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <table>
                    <tr>
                        <td colspan="2">Please enter your username and password
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>Username: </td>
                        <td>
                            <asp:TextBox ID="usernameTxt" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>Passwprd: </td>
                        <td>
                            <asp:TextBox ID="passTxt" runat="server" TextMode="Password"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:LinkButton ID="LinkButton1" runat="server">Forgot password?</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="Button2" runat="server" Text="Login" Width="150" OnClick="Button2_Click" />
                        </td>
                    </tr>
                </table>
            </td>
            <td width="6" height="162">
                <img src="imgs/style/seperator.png" /></td>
            <td valign="top">
                <table>
                    <tr>
                        <td align="left">If you don't have an account yet, please register. By registering you will have all the features that the website offers</td>
                    </tr>
                    <tr>
                        <td><hr /></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="Button1" runat="server" Text="Register" OnClick="Button1_Click" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label Visible="false" ID="errorLabels" runat="server" Text=""></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>

