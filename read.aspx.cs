﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class read : System.Web.UI.Page
{
    string recever = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (Session["username"] == null)
        {
            Response.Redirect("login.aspx");
        }
        else
        {
            if (Request.QueryString["id"] != null)
            {
                string s = "SELECT * FROM [messages] WHERE ID=" + Request.QueryString["id"].ToString() + " AND (username='" + Session["username"].ToString() + "' OR receiver='" + Session["username"].ToString() + "')";
                if (SQL.SelectRecords(s).Tables[0].Rows.Count > 0)
                {
                    string title, message, avatar ="" , sQ;
                    title = SQL.SelectRecords(s).Tables[0].Rows[0]["title"].ToString();
                    message = SQL.SelectRecords(s).Tables[0].Rows[0]["message"].ToString();
                    recever = SQL.SelectRecords(s).Tables[0].Rows[0]["username"].ToString();

                    sQ = "SELECT * FROM users WHERE username='" + Session["username"].ToString() + "'";
                    if (SQL.SelectRecords(sQ).Tables[0].Rows.Count > 0)
                    {
                        avatar = SQL.SelectRecords(sQ).Tables[0].Rows[0]["avatar"].ToString();
                    }
                    Label6.Text = title;
                    LinkButton1.Text = Session["username"].ToString();
                    LinkButton1.PostBackUrl = "member.aspx?user=" + Session["username"].ToString();
                    Image1.ImageUrl = Constants.AVATARS_IMAGE_PATH + avatar;
                    Label5.Text = message;

                    string se = "UPDATE [messages] SET [read]='1' WHERE ID=" + Request.QueryString["id"].ToString();
                    SQL.DoCommand(se);
                }
                else
                {
                    Session["error"] = "No message with the choosen id was found.";
                    Response.Redirect("error.aspx");
                }
            }
            else
            {
                Session["error"] = "Error loading the message.";
                Response.Redirect("error.aspx");
            }
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect(LinkButton1.PostBackUrl);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        if (Session["username"] != null)
        {
            if (this.TextBox1.Text.Length > 0 || !this.TextBox1.Text.Equals(""))
            {
                string title;
                title = Label6.Text;

                string q = "INSERT INTO messages([username],[receiver],[read],[title],[message]) VALUES('" + Session["username"].ToString() + "','" + recever + "','0','RE:" + title + "','" + TextBox1.Text + "')";
                SQL.DoCommand(q);
                Session["mail"] = "outbox";
                Response.Redirect("mail.aspx");
            }
        }
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("mail.aspx");
    }
}