﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="member.aspx.cs" Inherits="member" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="background-color: RGB(238,238,238); padding: 10px; border-right: 1px solid gray; border-left: 1px solid gray;" cellpadding="0" cellspacing="0" width="766">
        <tr>
            <td colspan="2" height="38" style="background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">

                <asp:Label ID="Label4" runat="server" Text="PETER's Profile"></asp:Label>

            </td>
        </tr>
        <tr>
            <td width="150" height="150">
                <asp:Image ID="Image1" runat="server" Height="150px" Width="150px" />
            </td>
            <td valign="top">
                <table width="100%" height="150" style="border:dashed 1px #2e92ff;">
                    <tr>
                        <td align="left" bgcolor="white">Full name:
                            <asp:Label ID="name" runat="server" Text="Label"></asp:Label>
                        </td>
                        <td align="left" bgcolor="white">Age:<asp:Label ID="age" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="white">Gender: 
                            <asp:Label ID="gender" runat="server" Text="gender"></asp:Label>
                        </td>
                        <td align="left" bgcolor="white">Country:<asp:Label ID="country" runat="server" Text="country"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" bgcolor="white">Email:<asp:Label ID="email" runat="server" Text="email"></asp:Label>
                        </td>
                        <td align="left"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" style="border:dashed 1px #2e92ff;">
                    <tr>
                        <td style="background-color: #00a8ff;">VISITOR MESSGAGES
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Panel1" runat="server">
                                <table width="100%">
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="TextBox3" runat="server" Height="50px" TextMode="MultiLine" Width="630px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right">
                                            <asp:Button ID="Button2" runat="server" Text="Send visitor message" OnClick="Button2_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:DataList Width="100%" ID="DataList1" runat="server" DataKeyField="sender" OnItemCommand="DataList1_ItemCommand">
                                <ItemTemplate>
                                    <table align="left" bgcolor="#dcdcdc" style="border: dashed darkgray 1px; table-layout: fixed;">
                                        <tr>
                                            <td align="left">From: <asp:LinkButton runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "sender") %>' CommandName="select"></asp:LinkButton></td>
                                            <td align="right">Date: <%#DataBinder.Eval(Container.DataItem, "msgdate") %></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 735px;" colspan="2" align="left">
                                                <%#DataBinder.Eval(Container.DataItem, "message") %>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

