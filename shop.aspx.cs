﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


public partial class shop : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (!this.IsPostBack)
        {
            Session["cat"] = "*"; 
            BindDataList();
            BindDataList1();
        }
    }

    public void BindDataList()
    {
        String statement = "SELECT * FROM items" + ((Session["cat"].ToString() == "*")? "" : " WHERE catid=" +int.Parse(Session["cat"].ToString())).ToString();
        this.DataList2.DataSource = SQL.SelectRecords(statement).Tables[0];
        this.DataList2.DataBind();
    }

    public void BindDataList1()
    {
        String statement = "SELECT * FROM cats";
        this.DataList1.DataSource = SQL.SelectRecords(statement).Tables[0];
        this.DataList1.DataBind();
    }

    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        this.DataList1.SelectedIndex = e.Item.ItemIndex;
        if(this.DataList1.DataKeys[e.Item.ItemIndex].ToString().Equals("1")) {
            Session["cat"] = "*";
        }  else {
             Session["cat"] = this.DataList1.DataKeys[e.Item.ItemIndex].ToString();
        }
        BindDataList();
    }
    protected void DataList2_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if(e.CommandName=="select")
        Response.Redirect("item.aspx?id="+this.DataList2.DataKeys[e.Item.ItemIndex]);
    }
}