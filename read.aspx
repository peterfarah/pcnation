﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="read.aspx.cs" Inherits="read" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="background-color: RGB(238,238,238); padding-left: 15px; padding-right: 15px; padding-top: 15px; border-right: 1px solid gray; border-left: 1px solid gray;" width="766" celpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table width="95%" style="border: dashed 1px #1887ff;">
                    <tr>
                        <td colspan="2">
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click"><- Go back to mail</asp:LinkButton></td>
                    </tr>
                    <tr>
                        <td colspan="2" height="38" style="padding-left: 15px; padding-left: 5px; background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">
                            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr style="border-bottom:solid 1px gray;">
                        <td width="180" style="border-right: solid 1px gray;">
                            <table>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Username</asp:LinkButton></td>
                                </tr>
                                <tr>
                                    <td width="150" height="150">
                                        <asp:Image ID="Image1" runat="server" Height="150px" Width="150px" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td valign="top" align="left" style="padding-top: 10px; padding-left: 10px;">
                            <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <table style="margin:15px; border: dashed 1px #1887ff;" width="75%">
                                <tr>
                                    <td height="38" style="padding-left: 15px; padding-left: 5px; background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">
                                        Reply to message
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server" Width="99%" Height="150" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button2" runat="server" Text="Send reply" Width="200px" OnClick="Button2_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

