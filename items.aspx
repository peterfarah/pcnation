﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="items.aspx.cs" Inherits="items" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="center" style="background: url('imgs\/style\/adminbody.png');" width="788" align="center">
        <tr>
            <td colspan="3" align="center">ITEMS CONTROL PANEL
            </td>
        </tr>
        <tr>
            <td align="right">Item name</td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox> <asp:Button ID="Button1" runat="server" Text="Search" OnClick="Button1_Click" />
            
            </td>
            <td>
                <asp:LinkButton ID="LinkButton4" runat="server" OnClick="LinkButton4_Click">Add new Item</asp:LinkButton>
                </td>
        </tr>
        <tr>
            <td align="center" colspan="3" style="padding:10px; border:solid 1px gray;">
                <asp:DataList ID="DataList1" runat="server" Width="75%" DataKeyField="ID" OnDeleteCommand="DataList1_DeleteCommand" OnEditCommand="DataList1_EditCommand">
                    <ItemTemplate>
                        <table align="left" width="100%">
                            <tr>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "ID") %>
                                </td>
                                <td>
                                    <%#DataBinder.Eval(Container.DataItem, "itemname") %>
                                </td>
                                <td align="right">
                                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="edit">Edit</asp:LinkButton>
                                    |
                                    <asp:LinkButton ID="LinkButton2" runat="server" CommandName="delete">Delete</asp:LinkButton>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </td>
        </tr>
    </table>
</asp:Content>

