﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.IO;

public partial class memberedit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admincp"] != null)
        {
            SortedDictionary<string, string> objDic = new SortedDictionary<string, string>();

            foreach (CultureInfo ObjectCultureInfo in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                RegionInfo objRegionInfo = new RegionInfo(ObjectCultureInfo.Name);
                if (!objDic.ContainsKey(objRegionInfo.EnglishName))
                {
                    objDic.Add(objRegionInfo.EnglishName, ObjectCultureInfo.Name);
                }
            }

            foreach (KeyValuePair<string, string> val in objDic)
            {
                this.DropDownList1.Items.Add(new ListItem(val.Key, val.Value));
            }

            if (Request.QueryString["user"] != null)
            {
                string s = "SELECT * FROM users WHERE username='" + Request.QueryString["user"] + "'";
                string firstname, lastname, password, age, email;
                if (SQL.SelectRecords(s).Tables[0].Rows.Count > 0)
                {
                    if (!IsPostBack)
                    {
                        firstname = SQL.SelectRecords(s).Tables[0].Rows[0]["firstname"].ToString();
                        lastname = SQL.SelectRecords(s).Tables[0].Rows[0]["lastname"].ToString();
                        email = SQL.SelectRecords(s).Tables[0].Rows[0]["email"].ToString();
                        age = SQL.SelectRecords(s).Tables[0].Rows[0]["age"].ToString();
                        password = SQL.SelectRecords(s).Tables[0].Rows[0]["password"].ToString();

                        TextBox1.Text = firstname;
                        TextBox2.Text = lastname;
                        TextBox3.Text = email;
                        TextBox4.Text = age;
                        TextBox5.Text = password;
                    }
                }
            }
        }
        else
        {
            Response.Redirect("alogin.aspx");
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string s = "";
        if (this.FileUpload1.PostedFile.ContentLength != 0)
        {
            s = "UPDATE users SET firstname='" + TextBox1.Text + "',lastname='" + TextBox2.Text + "',[email]='" + TextBox3.Text + "',[age]=" + TextBox4.Text + ",[password]='" + TextBox5.Text + "',[country]='" + DropDownList1.SelectedItem + "',gender='" + DropDownList2.SelectedValue + "',avatar='" + FileUpload1.FileName + "' WHERE username='" + Request.QueryString["user"] + "'";
            string destDir = Server.MapPath("./imgs/avatars/");
            string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
            string desPath = Path.Combine(destDir, fileName);
            FileUpload1.PostedFile.SaveAs(desPath);
            SQL.DoCommand(s);
            Response.Redirect("members.aspx");
        }
        else
        {
            s = "UPDATE users SET firstname='" + TextBox1.Text + "',lastname='" + TextBox2.Text + "',[email]='" + TextBox3.Text + "',[age]=" + TextBox4.Text + ",[password]='" + TextBox5.Text + "',[country]='" + DropDownList1.SelectedItem + "',gender='" + DropDownList2.SelectedValue + "' WHERE username='" + Request.QueryString["user"] + "'";
            SQL.DoCommand(s);
            Response.Redirect("members.aspx");
        }
    }
    private void bind()
    {
        string s = "SELECT * FROM visitors WHERE username='" + Request.QueryString["user"]+"'";
        DataList1.DataSource = SQL.SelectRecords(s).Tables[0];
        DataList1.DataBind();

    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        bind();
    }
    protected void DataList1_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        string s = "DELETE FROM visitors WHERE ID=" + DataList1.DataKeys[e.Item.ItemIndex];
        SQL.DoCommand(s);
        bind();
    }
}