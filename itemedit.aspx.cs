﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class itemedit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admincp"] != null)
        {
            if (Request.QueryString["id"] != null)
            {
                string s = "SELECT * FROM items WHERE ID=" + Request.QueryString["id"];
                string name, desc, price;
                if (SQL.SelectRecords(s).Tables[0].Rows.Count > 0)
                {
                    if (!IsPostBack)
                    {
                        name = SQL.SelectRecords(s).Tables[0].Rows[0]["itemname"].ToString();
                        desc = SQL.SelectRecords(s).Tables[0].Rows[0]["itemdesc"].ToString();
                        price = SQL.SelectRecords(s).Tables[0].Rows[0]["price"].ToString();

                        TextBox1.Text = name;
                        TextBox2.Text = price;
                        TextBox4.Text = desc;
                    }
                }
                Button1.Text = "Update information";
                LinkButton2.Visible = true;
            }
            else
            {
                Button1.Text = "Add Item";
                LinkButton2.Visible = false;
            }
        }
        else
        {
            Response.Redirect("alogin.aspx");
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] != null)
        {
            string s = "";
            if (this.FileUpload1.PostedFile.ContentLength != 0)
            {
                s = "UPDATE items SET itemname='" + TextBox1.Text + "',itemdesc='" + TextBox4.Text + "',price=" + TextBox2.Text + ",catid=" + DropDownList1.SelectedValue + ",itempic='" + FileUpload1.FileName + "' WHERE ID=" + Request.QueryString["id"];
                string destDir = Server.MapPath("./imgs/items/");
                string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
                string desPath = Path.Combine(destDir, fileName);
                FileUpload1.PostedFile.SaveAs(desPath);

                SQL.DoCommand(s);
                Response.Redirect("items.aspx");
            }
            else
            {
                s = "UPDATE items SET itemname='" + TextBox1.Text + "',itemdesc='" + TextBox4.Text + "',price=" + TextBox2.Text + ",catid=" + DropDownList1.SelectedValue + " WHERE ID=" + Request.QueryString["id"];
                SQL.DoCommand(s);
                Response.Redirect("items.aspx");
            }
        }
        else
        {
            string s = "INSERT INTO [items] ([itemname],[itemdesc],[price],[itempic],[catid]) VALUES('" + TextBox1.Text + "','" + TextBox4.Text + "'," + TextBox2.Text + ",'" + FileUpload1.FileName + "'," + DropDownList1.SelectedValue + ")";
            string destDir = Server.MapPath("./imgs/items/");
            string fileName = Path.GetFileName(FileUpload1.PostedFile.FileName);
            string desPath = Path.Combine(destDir, fileName);
            FileUpload1.PostedFile.SaveAs(desPath);
            SQL.DoCommand(s);
            Response.Redirect("items.aspx");
        }
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        bind();
    }

    private void bind()
    {
        string s = "SELECT * FROM comments WHERE itemid=" + Request.QueryString["id"];
        DataList1.DataSource = SQL.SelectRecords(s).Tables[0];
        DataList1.DataBind();
    }
    protected void DataList1_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        string s = "DELETE FROM comments WHERE ID=" + DataList1.DataKeys[e.Item.ItemIndex];
        SQL.DoCommand(s);
        bind();
    }
}