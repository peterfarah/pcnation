﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using currency;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        cartPanel.Visible = false;
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (Session["username"] != null)
        {
            loginPanel.Visible = false;
            cartPanel.Visible = true;
            LinkButton3.Text = "Logout";
            LinkButton3.PostBackUrl = "logout.aspx";
            if (!IsPostBack)
            {
                getInfo();
            }
        }
        else
        {
            LinkButton3.Text = "Login";
            LinkButton3.PostBackUrl = "login.aspx";
            loginPanel.Visible = true;
            cartPanel.Visible = false;
        }
       /*try
        {
            if (!this.IsPostBack)
            {
                CurrencyConvertor CurrencyWS = new CurrencyConvertor();
                string usd = CurrencyWS.ConversionRate(Currency.USD, Currency.ILS).ToString();
                string euro = CurrencyWS.ConversionRate(Currency.EUR, Currency.ILS).ToString();
                usdlabel.Text = usd;
                eurolbl.Text = euro;
            }
        }
        catch (Exception ex)
        {
            
        }*/

    }

    public void getInfo()
    {
        string usernamei = "", avatari = "", admin;
        int readmessage = 0, unreadmessages = 0;

        DataSet results = SQL.SelectRecords("SELECT * FROM users WHERE username='" + Session["username"].ToString() + "'");
        if (results.Tables[0].Rows.Count > 0)
        {
            usernamei = results.Tables[0].Rows[0]["username"].ToString();
            avatari = results.Tables[0].Rows[0]["avatar"].ToString();
            admin = results.Tables[0].Rows[0]["admin"].ToString();
            if (admin == "1")
            {
                Session["admin"] = "ok";
                Label1.Visible = true;
            }
            else
            {
                Session["admin"] = null;
                Label1.Visible = false;

            }

        }

        DataSet messages = SQL.SelectRecords("SELECT * FROM [messages] WHERE receiver='" + Session["username"].ToString() + "'");
        if (messages.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow row in messages.Tables[0].Rows)
            {
                if (int.Parse(row["read"].ToString()) == 1)
                    readmessage++;
                else if (int.Parse(row["read"].ToString()) == 0)
                    unreadmessages++;
            }
        }
        lblMsgs.Text = unreadmessages.ToString() ;
        this.LinkButton6.Text = usernamei;

        avatarImg.ImageUrl = Constants.AVATARS_IMAGE_PATH + avatari;

        string statement = "SELECT * FROM cart WHERE username='" + Session["username"].ToString() + "' AND visible=0";
        DataSet set = SQL.SelectRecords(statement);
        int total = 0;
        if (set.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow row in set.Tables[0].Rows)
            {
                int price = int.Parse(set.Tables[0].Rows[0]["price"].ToString());
                int qty = int.Parse(set.Tables[0].Rows[0]["qty"].ToString());
                total += (qty * price);
            }
        }
        Label4.Text = "$" +total + "";
        LinkButton5.Text = set.Tables[0].Rows.Count+"";

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string statement = "SELECT * FROM users WHERE [username]='" + this.TextBox1.Text + "' AND [password]='" + this.TextBox2.Text + "'";
        if (SQL.SelectRecords(statement).Tables[0].Rows.Count > 0)
        {
            Session["username"] = this.TextBox1.Text;
            if (int.Parse(SQL.SelectRecords(statement).Tables[0].Rows[0]["admin"].ToString()) == 1)
            {
                Session["admin"] = "ok";
                Label1.Visible = true;
            }
            Response.Redirect(Request.RawUrl);

        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Response.Redirect("shop.aspx");
    }
    protected void LinkButton2_Click(object sender, EventArgs e)
    {
        Response.Redirect("shop.aspx");
    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        Response.Redirect("About us.aspx");
    }
    protected void LinkButton3_Click(object sender, EventArgs e)
    {
        Response.Redirect(LinkButton3.PostBackUrl);
    }
    protected void Label1_Click(object sender, EventArgs e)
    {
        Response.Redirect("alogin.aspx");
    }
    protected void LinkButton5_Click(object sender, EventArgs e)
    {
        Response.Redirect("cart.aspx");
    }
    protected void LinkButton6_Click(object sender, EventArgs e)
    {
        Response.Redirect("member.aspx?user=" + LinkButton6.Text);
    }

    protected void lblMsgs_Click1(object sender, EventArgs e)
    {
        Response.Redirect("mail.aspx");
    }
}
