﻿<%@ Page Title="" Language="C#" MasterPageFile="~/admin.master" AutoEventWireup="true" CodeFile="memberedit.aspx.cs" Inherits="memberedit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="background: url('imgs\/style\/adminbody.png');" width="788" align="center">
        <tr>
            <td align="center">
                <table width="85%" >
                    <tr>
                      <td height="38" style="background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">
                          USER MANEGMENT
                      </td>  
                    </tr>
                    <tr>
                        <td align="center">
                            <table>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                    </td><td>
                                        <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBox5" runat="server" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="TextBox3" runat="server" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:FileUpload ID="FileUpload1" runat="server" Width="100%"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">

                                        <asp:DropDownList ID="DropDownList1" runat="server" Width="100%">
                                        </asp:DropDownList>

                                    </td>
                                </tr>
                                <tr>
                                    <td>

                                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>

                                    </td>
                                    <td>
                                        <asp:DropDownList ID="DropDownList2" runat="server">
                                            <asp:ListItem>Male</asp:ListItem>
                                            <asp:ListItem>Female</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="Button1" runat="server" Text="Update information" OnClick="Button1_Click" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td height="38" style="background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">
                            ITEM REVIEW MANEGMENT
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">Manage reviews</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">

                            <asp:DataList ID="DataList1" runat="server" Width="100%" DataKeyField="ID" OnDeleteCommand="DataList1_DeleteCommand">
                                <ItemTemplate>
                                    <table align="left" width="100%">
                                        <tr>
                                            <td align="left">
                                                 Sender: <%#DataBinder.Eval(Container.DataItem, "sender") %>
                                            </td>
                                            <td align="left">
                                                message: <%#DataBinder.Eval(Container.DataItem, "message") %>
                                            </td>
                                            <td align="left">
                                                date: <%#DataBinder.Eval(Container.DataItem, "msgdate") %>
                                            </td>
                                            <td align="right">
                                                <asp:LinkButton ID="LinkButton1" CommandName="delete" runat="server">DELETE</asp:LinkButton>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                            <hr /></td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

