﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.OleDb;

public partial class item : System.Web.UI.Page
{
    private int ITEM_ID;

    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (Session["username"] != null)
        {
            Button2.Visible = true;
            Panel1.Visible = true;
        }
        else
        {
            Button2.Visible = false;
            Panel1.Visible = false;
        }
        if (Request.QueryString["id"] != null)
        {
            ITEM_ID = int.Parse(Request.QueryString["id"]);

            DataSet results = SQL.SelectRecords("SELECT * FROM items WHERE ID=" + ITEM_ID);
            if (results.Tables[0].Rows.Count > 0)
            {
                nameLabel.Text = results.Tables[0].Rows[0]["itemname"].ToString();
                descLabel.Text = results.Tables[0].Rows[0]["itemdesc"].ToString();
                priceLabel.Text = results.Tables[0].Rows[0]["price"].ToString();
                Image1.ImageUrl = Constants.ITEMS_IMAGE_PATH + results.Tables[0].Rows[0]["itempic"].ToString();

                bindCommentList();
            }
            else
            {
                Session["error"] = "The member you are looking for doesn't exist.";
                Response.Redirect("error.aspx");
            }
        }
        else
        {
            Session["error"] = "The item you are looking for doesn't exist.";
            Response.Redirect("error.aspx");
        }
    }

    public void bindCommentList()
    {
        String statement = "SELECT * FROM comments WHERE itemid=" + ITEM_ID;
        this.DataList1.DataSource = SQL.SelectRecords(statement).Tables[0];
        this.DataList1.DataBind();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (TextBox3.Text.Length.Equals("") || TextBox3.Text.Length == 0)
            return;
        string statement = "INSERT INTO comments (itemid,comment,username,commentdate) VALUES(" + ITEM_ID + ",'" + TextBox3.Text + "','" + Session["username"].ToString() + "','" + (DateTime.Today.ToString()) + "')";
        SQL.DoCommand(statement);
        bindCommentList();
        TextBox3.Text = "";
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        Response.Redirect("member.aspx?user=" + this.DataList1.DataKeys[e.Item.ItemIndex]);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        if (Session["username"] != null)
        {
            string query = "SELECT * FROM cart WHERE itemid=" + ITEM_ID + " AND username='" + Session["username"].ToString() + "'";
            if (SQL.SelectRecords(query).Tables[0].Rows.Count > 0)
            {
                int qty = int.Parse(SQL.SelectRecords(query).Tables[0].Rows[0]["qty"].ToString());
                qty++;
                SQL.DoCommand("UPDATE cart SET qty=" + qty + " WHERE username='" + Session["username"].ToString() + "' AND itemid=" + ITEM_ID);
            }
            else
            {
                string s = "INSERT INTO cart(username,itemid,qty,visible,itemname,price) VALUES('" + Session["username"].ToString() + "'," + ITEM_ID + ",1,0,'" + nameLabel.Text + "'," + int.Parse(priceLabel.Text) + ")";
                SQL.DoCommand(s);
            }
        }
    }
}