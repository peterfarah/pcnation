﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="Default2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .labels {
            color:RGB(0,138,233);
            font-family:'Times New Roman';
            font-size:16px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <table style="background-color: RGB(238,238,238); padding-bottom: 4px; border-right: 1px solid gray; border-left: 1px solid gray;" width="766">
        <tr>
            <td colspan="2" align="center">
                <table style="background-color:#dedede; border:solid 1px #9e9e9e;" cellpadding="5" cellspacing="5">
                    <tr>
                        <td><span class="labels">Username*:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox3" runat="server" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Username field is required." ControlToValidate="TextBox3" SetFocusOnError="False"></asp:RequiredFieldValidator></td>
                    </tr>

                    <tr>
                        <td><span class="labels">Password*:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox4" runat="server" TextMode="Password" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox></td><td align="left"><asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Password field is required." ControlToValidate="TextBox4"></asp:RequiredFieldValidator></td>
                    </tr>
                    <tr>
                        <td><span class="labels">Confirm Password*:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox5" runat="server" TextMode="Password" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Confirm password field is required" ControlToValidate="TextBox5"></asp:RequiredFieldValidator>
                            </td>
                    </tr>




                    <tr>
                        <td><span class="labels">Email*:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox6" runat="server" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="E-mail field is required." ControlToValidate="TextBox6"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox6" ErrorMessage="Please enter a valid E-mail." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                    </tr>


                    <tr>
                        <td><span class="labels">First name*:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox7" runat="server" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="First name field is required" ControlToValidate="TextBox7"></asp:RequiredFieldValidator></td>
                    </tr>



                    <tr>
                        <td><span class="labels">Last name*:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox8" runat="server" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox></td>
                        <td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Last name field is required" ControlToValidate="TextBox8"></asp:RequiredFieldValidator></td>
                    </tr>



                    <tr>
                        <td><span class="labels">Gender*:</span>
                            
                        </td>
                        <td>
                            <asp:RadioButton ID="RadioButton1" runat="server" GroupName="gender" Text="Male" Checked="True" />
                            <asp:RadioButton ID="RadioButton2" runat="server" GroupName="gender" Text="Female" /></td><td></td>
                    </tr>
                    <tr>
                        <td><span class="labels">Country*:</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="DropDownList2" runat="server">
                            </asp:DropDownList>
                        </td><td></td>
                    </tr>
                    <tr>
                        <td><span class="labels">Age*:</span>
                        </td>
                        <td>
                            <asp:TextBox ID="TextBox9" runat="server" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox></td><td align="left">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="TextBox9" ErrorMessage="You have to set your age"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Age has to be a numeric value (0-120)" ControlToValidate="TextBox9" MaximumValue="120" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr />
                            </td>
                    </tr>
                    <tr>
                        <td><span class="labels">Validation</span></td>
                        <td>
                            <asp:TextBox Width="70" ID="TextBox1" runat="server" BorderColor="#3399FF" BorderStyle="Solid" BorderWidth="1px"></asp:TextBox>
                            <asp:Label ID="Label4" runat="server" BackColor="Black" Font-Bold="True" Font-Size="Large" ForeColor="White" EnableTheming="True"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" align="left"><asp:CheckBox ID="CheckBox1" CssClass="labels" runat="server" Text="I'v read and agreed on trems of service." /></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="Panel1" runat="server" BorderColor="#A7A7A7" BackColor="#F4F4F4" BorderStyle="Solid" BorderWidth="1px">
                                <table align="center">

                                    <tr>
                                        <td>
                                            <asp:Label ID="errors" runat="server" Text="Label" ForeColor="Red"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Button ID="Button1" runat="server" Text="Register" OnClick="Button1_Click" /><asp:Button ID="Button2" runat="server" Text="Cancel" /></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

