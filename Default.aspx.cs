﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        getStatics();
        bindNewsList();
    }

    private void bindNewsList()
    {
        string s = "SELECT * FROM [news] ORDER BY ID DESC";
        DataList1.DataSource = SQL.SelectRecords(s);
        DataList1.DataBind();
    }

    private void getStatics()
    {
        Label1.Text = SQL.SelectRecords("SELECT * FROM users").Tables[0].Rows.Count + " Registered user";
        Label2.Text = SQL.SelectRecords("SELECT * FROM items").Tables[0].Rows.Count + " items in the shop";
    }
}