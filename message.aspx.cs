﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class message : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (Session["username"] == null)
        {
            Response.Redirect("login.aspx");
        }
        if (Request.QueryString["users"] != null)
        {
            TextBox3.Text = Request.QueryString["users"].ToString();
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (TextBox3.Text.Length == 0 || TextBox4.Text.Length == 0 || TextBox5.Text.Length == 0)
        {
            return;
        }
        string[] to = TextBox3.Text.Split(new char[] { ',' });
        if (to.Length > 1)
        {
            foreach (string name in to)
            {
                string statement = "INSERT INTO [messages] ([username],[receiver],[read],[title],[message]) VALUES('" + Session["username"].ToString() + "','" + name + "','0','" + TextBox4.Text + "','" + TextBox5.Text + "')";
                SQL.DoCommand(statement);
            }
        }
        else 
        {
            string statement = "INSERT INTO [messages] ([username],[receiver],[read],[title],[message]) VALUES('" + Session["username"].ToString() + "','" + TextBox3.Text + "','0','" + TextBox4.Text + "','" + TextBox5.Text + "')";
            SQL.DoCommand(statement);
        }
        Session["mail"] = "outbox";
        Response.Redirect("mail.aspx");
    }
    protected void LinkButton7_Click(object sender, EventArgs e)
    {
        Response.Redirect("mail.aspx");
    }
}