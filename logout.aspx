﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="logout.aspx.cs" Inherits="logout" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <table style="background-color:RGB(238,238,238); padding-bottom:4px; padding-top:15px;" border-right:1px solid gray; border-left:1px solid gray;" cellpadding="0" cellspacing="0" width="766">
        <tr>
            <td align="center">
                <table width="500" style="border: solid 1px gray;">
                    <tr>
                        <td height="38" style="background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">
                            Logout
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="margin:15px; border:dashed 1px #007ffa;" height="50" width="450">
                                <tr>
                                    <td>
                                        You will be redirected to home in 5 seconds<a href="shop.aspx"> Return home</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
    </table>
</asp:Content>

