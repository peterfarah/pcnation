﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class cart : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SQL.initConnection(Server.MapPath("./App_Data/"));
        if (Session["username"] != null)
        {
            bindCartList();
        }
        else
        {
            Response.Redirect("login.aspx");
        }
    }

    private void bindCartList()
    {
        string statement = "SELECT * FROM cart WHERE username='" + Session["username"].ToString() + "' AND visible=0";
        DataSet set = SQL.SelectRecords(statement);
        int total = 0;
        if (set.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow row in set.Tables[0].Rows)
            {
                int price = int.Parse(set.Tables[0].Rows[0]["price"].ToString());
                int qty = int.Parse(set.Tables[0].Rows[0]["qty"].ToString());
                total += (qty * price);
            }
        }
        Label4.Text = total + "";
        DataList1.DataSource = set.Tables[0];
        DataList1.DataBind();
    }

    protected void DataList1_DeleteCommand(object source, DataListCommandEventArgs e)
    {
        int id = int.Parse(DataList1.DataKeys[e.Item.ItemIndex].ToString());
        string s12 = "DELETE FROM cart WHERE ID=" + id;
        SQL.DoCommand(s12);
        bindCartList();
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "select")
        {
            int id = int.Parse(DataList1.DataKeys[e.Item.ItemIndex].ToString());
            string statement = "SELECT * FROM cart WHERE ID=" + id;
            DataSet res = SQL.SelectRecords(statement);
            if (res.Tables[0].Rows.Count > 0)
            {
                int qty = int.Parse(res.Tables[0].Rows[0]["qty"].ToString());
                qty++;
                string s = "UPDATE cart SET qty=" + qty + " WHERE ID=" + id;
                SQL.DoCommand(s);
                bindCartList();
            }
        }

    }
    protected void DataList1_EditCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "edit")
        {
            int id = int.Parse(DataList1.DataKeys[e.Item.ItemIndex].ToString());
            string statement = "SELECT * FROM cart WHERE ID=" + id;
            DataSet res = SQL.SelectRecords(statement);
            if (res.Tables[0].Rows.Count > 0)
            {
                int qty = int.Parse(res.Tables[0].Rows[0]["qty"].ToString());
                if (qty == 1)
                    qty = 1;
                else
                    qty--;
                string s1 = "UPDATE cart SET qty=" + qty + " WHERE ID=" + id;
                SQL.DoCommand(s1);
                bindCartList();
            }
        }
    }
}