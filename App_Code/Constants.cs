﻿using System;
using System.Collections.Generic;
using System.Web;

public class Constants
{

    //General Variables
    public static readonly string TITLE = "PCNation";

    //Paths Variables
    public static readonly string IMAGES_PATH = "./imgs/";

    public static readonly string AVATARS_IMAGE_PATH = IMAGES_PATH + "avatars/";

    public static readonly string ITEMS_IMAGE_PATH = IMAGES_PATH + "items/";
}