﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Util
/// </summary>
public class Util
{
    public static string RandomString(int length)
    {
        Random r = new Random(Environment.TickCount);
        string chars = "0123456789abcdefghijklmnopqrstuvwxyz";
        StringBuilder builder = new StringBuilder(length);
        for (int i = 0; i < length; ++i)
            builder.Append(chars[r.Next(chars.Length)]);
        return builder.ToString();
    }
}