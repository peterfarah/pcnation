﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data.OleDb;
using System.Data;

/// <summary>
/// Summary description for SQL
/// </summary>
public class SQL
{

    public static readonly String CONNECTION_DRIVER = @"Provider=Microsoft.Jet.OleDb.4.0;Data Source=";
    public static readonly String DATABASE_NAME = "db.mdb";

    private static OleDbConnection connection;

    public static void initConnection(String server)
    {
        connection = new OleDbConnection(CONNECTION_DRIVER + server + DATABASE_NAME);
    }

    public static void DoCommand(String Command)
    {
        OleDbCommand cmd = new OleDbCommand(Command, connection);
        connection.Open();
        cmd.ExecuteNonQuery();
        connection.Close();
    }

    public static DataSet SelectRecords(String Command)
    {
        OleDbDataAdapter daObj = new OleDbDataAdapter(Command, connection);
        DataSet dsObj = new DataSet();
        daObj.Fill(dsObj);
        return dsObj;
    }

}