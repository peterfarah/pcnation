﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="mail.aspx.cs" Inherits="mail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="background-color:RGB(238,238,238); padding-bottom:4px; padding-top:15px; border-right:1px solid gray; border-left:1px solid gray;" cellpadding="0" cellspacing="0" width="766">
        <tr>
            <td width="150" style="padding-left:15px;" valign="top">
                <table style="border: dashed 1px #1887ff;">
                    <tr>
                        <td height="38" style="padding-left:5px; background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">Nnew message</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:LinkButton ID="LinkButton2" runat="server" OnClick="LinkButton2_Click">Send new message</asp:LinkButton></td>
                    </tr>
                   <tr>
                       <td height="38" style="padding-left:5px; background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">Choose Inbox/Outbox  </td>
                   </tr>
                    <tr>
                        <td>

                            <asp:LinkButton ID="LinkButton7" runat="server" OnClick="LinkButton7_Click">Inobx</asp:LinkButton>

                        </td>
                    </tr>
                    <td>

                        <asp:LinkButton ID="LinkButton8" runat="server" OnClick="LinkButton8_Click">Outbox</asp:LinkButton>

                    </td>
                </table>
            </td>
            <td align="center" valign="top">
               <table width="95%" style="border: dashed 1px #1887ff;">
                   <tr>
                      <td height="38" style="padding-left:5px; background-image: url('./imgs/style/logintitle.png'); background-repeat: repeat-x;">Messages</td>
                   </tr>
                   <tr>
                       <td>

                           <asp:DataList ID="DataList1" runat="server" Width="100%" DataKeyField="ID" OnItemCommand="DataList1_ItemCommand">
                               <ItemTemplate>
                                   <table width="100%">
                                       <tr>
                                           <td width="75%" align="left">
                                               <asp:LinkButton ID="LinkButton1" CommandName="select" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "title") %>'></asp:LinkButton></td>
                                           <td>FROM: <%#DataBinder.Eval(Container.DataItem, "username") %></td>
                                           <td>TO: <%#DataBinder.Eval(Container.DataItem, "receiver") %></td>
                                       </tr>
                                       <tr>
                                           <td colspan="3">
                                               <hr />
                                           </td>
                                       </tr>
                                   </table>
                               </ItemTemplate>
                           </asp:DataList>
                       </td>
                   </tr>
               </table>
            </td>
        </tr>
    </table>
</asp:Content>

